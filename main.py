from http.server import BaseHTTPRequestHandler, HTTPServer
import re
from currency import BankInfo, Currency


def prepareHtml(charCodes):
    mainPage = open("bootsTest.html")
    options = ''
    for charCode in charCodes:
        options += "<option value=%(charCode)s>%(charCode)s</option>" % {'charCode': charCode}
    mainPage = open("bootsTest.html").read()
    marker = "[OPTIONS_HERE]"
    mainPage = mainPage.replace(marker, options)
    return mainPage


class S(BaseHTTPRequestHandler):
    bankInfo = BankInfo()

    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        # print("GET")
        self._set_headers()
        mainPage = prepareHtml(self.bankInfo.charCodes())
        self.wfile.write(bytearray(mainPage, 'utf-8'))

    def do_HEAD(self):
        self._set_headers()

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_length)
        data = post_data.decode('utf-8')
        search = re.search('@(.+?)@', data)
        if search:
            data = search.group(1).split(',')
        # print(data)
        response = 0
        if (len(data) == 3):
            if(data[2] != ''):
                if ((data[0] in self.bankInfo.charCodes()) &
                        (data[1] in self.bankInfo.charCodes())
                        & (float(data[2]) > 0)):
                    response = self.bankInfo.convert(data[0], data[1], float(data[2]))
        self._set_headers()
        # print(response)
        self.wfile.write(bytearray(str(response), 'utf-8'))


def run(server_class=HTTPServer, handler_class=S, port=80):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print('Starting httpd...')
    httpd.serve_forever()


if __name__ == "__main__":
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
