import requests
import sys
import os
import xml.etree.ElementTree as ET
import datetime



class Currency():
    def __init__(self, currency=None):
        if(currency!=None):
            self._name = currency.find("Name").text
            self._charCode = currency.find("CharCode").text
            self._nominal = float(currency.find("Nominal").text.replace(",",'.'))
            self._value = float(currency.find("Value").text.replace(",",'.'))

class BankInfo():
    def __init__(self, parent=None):
        self.update()


    def update(self):
        #Retrive data from cbr
        self.lastTimeUpdated = datetime.datetime.now()
        url = "http://www.cbr.ru/scripts/XML_daily.asp?"
        r = requests.get(url)
        root = ET.fromstring(r.text)

        self.m_currencies = {}
        for neighbour in root:
            currency = Currency(currency= neighbour)
            self.m_currencies[currency._charCode] = currency
        currency = Currency()
        currency._name = "Российский рубль"
        currency._charCode = "RUB"
        currency._nominal = 1
        currency._value = 1
        self.m_currencies[currency._charCode] = currency
        print("Update!")


    m_convertedNum = 0
    def convertedNum(self):
        return self.m_convertedNum

    __chCodes = []
    def charCodes(self):
        if(not self.__chCodes):
            chCodes = []
            for charCode in self.m_currencies:
                chCodes.append(charCode)
            chCodes.sort()
            self.__chCodes = chCodes
            #put charCodes in html here?
        return self.__chCodes

    def convert(self, from_code, to_code, num):
        if(datetime.datetime.now() - self.lastTimeUpdated > datetime.timedelta(minutes=5)):
            self.update()
        fromCurrency = self.m_currencies[from_code]
        toCurrency = self.m_currencies[to_code]
        self.m_convertedNum = round(float(num) *
                                    (fromCurrency._value/fromCurrency._nominal) /
                                    (toCurrency._value/toCurrency._nominal), 5)
        # print(self.m_convertedNum)
        return self.m_convertedNum

# bankInfo = BankInfo()